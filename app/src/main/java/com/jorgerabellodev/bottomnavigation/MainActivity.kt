package com.jorgerabellodev.bottomnavigation

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        replaceFragment(CallsFragment.newInstance())

        bottom_navigation_view.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.bottombaritem_calls -> {
                    val fragment = CallsFragment.newInstance()
                    replaceFragment(fragment)
                }
                R.id.bottombaritem_messages -> {
                    val fragment = MessagesFragment.newInstance()
                    replaceFragment(fragment)
                }
                R.id.bottombaritem_games -> {
                    val fragment = GamesFragment.newInstance()
                    replaceFragment(fragment)
                }
            }
            true
        }

    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out)
            .replace(R.id.container, fragment, fragment.javaClass.simpleName)
            .addToBackStack(fragment.javaClass.simpleName)
            .commit()
    }
}
